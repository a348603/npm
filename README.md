# Mi primera aplicación con NPM

Pràctica

## Instrucciones

Vamos a crear nuestra primera aplicación con node.js utilizando dependencias.
1) Cree un directorio con nombre package_manager.
2) Entre al directorio y ejecute el comando para iniciar una aplicación de node con npm.
3) Instale las siguientes dependencias: Manejador de log's, herramienta de refresco en caliente, pruebas unitarias y lint.
4) Configure su proyecto con git correctamente.
5) Suba la liga de gitlab.

## Autor

* Daniel Josue Lozano Porras 348603
