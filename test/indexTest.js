const sumar = require('../index');
const assert = require('assert');

describe("Probar la suma de dos nùmeros", ()=>{
	// Afirmar que 5 + 5 es igual a 10
	it("Cinco màs cinco es 10", ()=>{
		assert.equal(10, sumar(5,5));
	});

	it("Cinco màs cinco no son 55", ()=>{
		assert.notEqual(55, sumar(5,5))
	})
});
