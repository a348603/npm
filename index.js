const log4js = require('log4js');

const logger = log4js.getLogger();
logger.level = "info";

logger.debug("Iniciando aplicaciòn en modo de pruebas.");
logger.info("La app ha sido iniciado correctamente");
logger.warn("Falta el archivo config en la app");
logger.error("NO se pudo acceder al sistema de archivos");
logger.fatal("Aplicaciòn no se pudo ejecutar en el so");

function sumar(x, y){
	return x + y;
}

module.exports = sumar;
